package com.paypani;

import com.paypani.db.entity.Brand;
import com.paypani.types.response.BrandServiceResponse;
import org.apache.commons.lang.StringUtils;

/**
 * Action t
 */
public class BrandService {

    public BrandServiceResponse add(Brand brand){
        BrandServiceResponse response = new BrandServiceResponse();
        try{
            if(brand == null || StringUtils.isBlank(brand.getName())){
                throw new IllegalStateException("Illegal parameter passed :: brand ==" + brand);
            }

        }catch(IllegalStateException ise){

        }
        response.setStatusCode(BrandServiceResponse.SUCCESS_STATUS_CODE);
        response.setStatusMsg(BrandServiceResponse.SUCCESS_STATUS_MSG);

        return response;
    }
}
