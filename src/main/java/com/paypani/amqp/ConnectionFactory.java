package com.paypani.amqp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Connection;

public class ConnectionFactory {
	
	private static final String propertyFileName = "rabbitmq.properties";
	
	private static final String vhost;
	private static final String server;
	private static final String username;
	private static final String password;
	
	private Channel channel = null;
	
	private static com.rabbitmq.client.ConnectionFactory factory;
	
	static{
		try{
			Properties props = new Properties();
			InputStream inputStream = ConnectionFactory.class.getClassLoader().getResourceAsStream(propertyFileName);
			if(inputStream == null)
				throw new RuntimeException("Can not find rabbitmq property file");
			
			props.load(inputStream);
			vhost = props.getProperty("vhost");
			server = props.getProperty("server");
			username = props.getProperty("username");
			password = props.getProperty("password");
			
			factory = new com.rabbitmq.client.ConnectionFactory();
			
			factory.setHost(server);
			factory.setUsername(username);
			factory.setPassword(password);
			factory.setVirtualHost(vhost);
		}catch(Exception ex){
			throw new ExceptionInInitializerError(ex.getMessage());
		}
	}
	
	private ConnectionFactory(String queueName){
		try{
			Connection connection = factory.newConnection();
			
			channel = connection.createChannel();
			channel.queueDeclare(queueName, true, false, false, null);
		}catch(Exception ex){
			throw new RuntimeException("unable to create connection", ex);
		}
	}
	
	public static ConnectionFactory getInstance(String QueueName){
		return new ConnectionFactory(QueueName);
	}
	
	
	public void publishDataToQueue(String exchange, String routingKey, BasicProperties props, byte[] body) throws IOException{
		channel.basicPublish(exchange, routingKey, props, body);
	} 
	
	public Channel getChannel(){
		return channel;
	}

}
