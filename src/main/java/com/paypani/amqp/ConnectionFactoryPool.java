package com.paypani.amqp;

import org.apache.commons.pool.PoolableObjectFactory;

public class ConnectionFactoryPool implements PoolableObjectFactory{
	
	private String queueName;
	
	public ConnectionFactoryPool(String queueName){
		this.queueName = queueName;
	}

	@Override
	public Object makeObject() throws Exception {
		return ConnectionFactory.getInstance(queueName);
	}

	@Override
	public void destroyObject(Object obj) throws Exception {
	}

	@Override
	public boolean validateObject(Object obj) {
		return true;
	}

	@Override
	public void activateObject(Object obj) throws Exception {
		
	}

	@Override
	public void passivateObject(Object obj) throws Exception {
	}

}
