package com.paypani.amqp;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

public class ConnectionObjectPool extends GenericObjectPool {
	
	private static final Logger log = Logger.getLogger(ConnectionObjectPool.class);

	public ConnectionObjectPool(ConnectionFactoryPool objFactory) {
		super(objFactory);
		this.setMaxIdle(2); // Maximum idle threads.
		this.setMaxActive(20); // Maximum active threads.
		this.setMinEvictableIdleTimeMillis(30000); // Evictor runs every 30
													// secs.
		this.setTestOnBorrow(true); // Check if the thread is still valid.
		// this.setMaxWait(1000); // Wait 1 second till a thread is available
	}

	public Object borrowObject() throws Exception {
		log.debug(" borrowing object..");
		return super.borrowObject();
	}

	public void returnObject(Object obj) throws Exception {
		log.debug(" returning object.." + obj);
		super.returnObject(obj);
	}

}
