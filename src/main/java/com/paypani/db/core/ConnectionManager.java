package com.paypani.db.core;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConnectionManager {

    private static EntityManager em;

    static{
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("paypani");
        em = emf.createEntityManager();
    }

    public static EntityManager getEM(){
        return em;
    }
}
