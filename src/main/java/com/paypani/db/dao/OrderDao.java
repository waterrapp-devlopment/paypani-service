package com.paypani.db.dao;

import java.util.List;

import com.paypani.types.Order;
import com.paypani.types.exception.GenericException;

public interface OrderDao {

	public Long createOrder(Order order);
	
	public void updateOrderStatus(Long orderId, Long statusId);

	public List<Order> getAllOrders() throws GenericException;

	void updateSmsCorrelationId(Long orderId, String correaltionId);
}
