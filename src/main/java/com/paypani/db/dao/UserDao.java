package com.paypani.db.dao;

import com.paypani.types.User;
import com.paypani.types.exception.DuplicateAccountException;
import com.paypani.types.exception.GenericException;
import com.paypani.types.exception.InvalidCredentialException;
import com.paypani.types.exception.InvalidUserException;

/**
 *
 */
public interface UserDao {

    public String validateUser(User user) throws InvalidCredentialException;
    
    public void createUser(User user) throws DuplicateAccountException;
    
    public boolean exists(String mobile);

	public User getUserByMobile(String mobile) throws GenericException;

	public User getUserByEmail(String email) throws GenericException;
	
	public void updatePassword(Long id, String passhash) throws InvalidUserException, GenericException;
}
