package com.paypani.db.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

import com.paypani.db.dao.OrderDao;
import com.paypani.db.entity.AddressEntity;
import com.paypani.db.entity.OrderEntity;
import com.paypani.db.entity.OrderStatus;
import com.paypani.db.entity.SMSStatus;
import com.paypani.service.util.BeanUtil;
import com.paypani.types.Address;
import com.paypani.types.Order;
import com.paypani.types.Order.Status;
import com.paypani.types.exception.GenericException;

public class OrderDaoImpl implements OrderDao{
	
	private static final Logger LOG = Logger.getLogger(OrderDaoImpl.class);
	
	private EntityManagerFactory emf;

	@Override
	public Long createOrder(Order order) {
		OrderEntity orderEntity = new OrderEntity();
		LOG.debug("inside createOrder method with order >> " + order);

		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			
			orderEntity.setCreationDate(new Date());
			orderEntity.setQuantity(order.getQuantity());
			orderEntity.setMobile(order.getMobile());
			AddressEntity addressEntity = new AddressEntity();
			//copy all properties to entity
			BeanUtil.copyProperties(order.getDeliveryAddress(), addressEntity, null, "com.paypani.db.entity");
			
			orderEntity.setAddressEntity(addressEntity);
			
			OrderStatus status = new OrderStatus();
			status.setStatusId(1l);
			
			SMSStatus smsStatus = new SMSStatus();
			smsStatus.setStatusId(1l);
			
			orderEntity.setSmsStatus(smsStatus);
			
			orderEntity.setOrderStatus(status);
			
			em.persist(orderEntity);
			em.getTransaction().commit();
		} catch (Exception ex) {
			if (em.getTransaction() != null && em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			LOG.error("unable to create order", ex);
			throw new RuntimeException("Error creating order for user :: " + order.getUserId());
		}
		return orderEntity.getOrderId();
	}

	private EntityManager getEntityManager() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("paypani");
		}
		return emf.createEntityManager();
	}
	
	@Override
	public void updateOrderStatus(Long orderId, Long statusId) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void updateSmsCorrelationId(Long orderId, String correaltionId){
		EntityManager em = getEntityManager();
		try{
			em.getTransaction().begin();
			OrderEntity orderEntity = em.find(OrderEntity.class, orderId);
			if(orderEntity == null)
				throw new RuntimeException("unable to find order with id");
			orderEntity.setSmsCorrealtion(correaltionId);
			em.getTransaction().commit();
		}catch(Exception ex){
			LOG.error("unable to update correaltion for order " + orderId);
		}finally{
			if(em.getTransaction() != null && em.getTransaction().isActive()){
				em.getTransaction().rollback();
			}
		}
	}
	
	@Override
	public List<Order> getAllOrders() throws GenericException{
		EntityManager em = getEntityManager();
		List<Order> orders = new ArrayList<Order>();
		try {
			em.getTransaction().begin();
			Collection<OrderEntity> ordersFromDB = em.createQuery("SELECT e FROM OrderEntity e").getResultList();
			if(orders != null){
				for(OrderEntity orderEntity : ordersFromDB){
					Order order = new Order();
					BeanUtil.copyProperties(orderEntity, order, null, "com.paypani.types");
					
					Address address = new Address();
					BeanUtil.copyProperties(orderEntity.getAddressEntity(), address, null, "com.paypani.types");
					order.setDeliveryAddress(address);
					order.setOrderStatus(Status.valueOf(orderEntity.getOrderStatus().getStatus()));
					orders.add(order);
				}
			}
			
		}catch(Exception ex){
			LOG.error("Excption loading orders", ex);
			throw new GenericException("Unable to load orders");
		}
		return orders;
		
	}

}
