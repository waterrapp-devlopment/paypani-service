package com.paypani.db.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.paypani.db.dao.UserDao;
import com.paypani.db.entity.AddressEntity;
import com.paypani.db.entity.UserDetailsEntity;
import com.paypani.db.entity.UserEntity;
import com.paypani.security.PasswordHandler;
import com.paypani.security.util.Base64;
import com.paypani.security.util.JWTTokenGenerator;
import com.paypani.service.util.BeanUtil;
import com.paypani.types.Address;
import com.paypani.types.User;
import com.paypani.types.UserDetails;
import com.paypani.types.exception.DuplicateAccountException;
import com.paypani.types.exception.GenericException;
import com.paypani.types.exception.InvalidCredentialException;
import com.paypani.types.exception.InvalidPasswordException;
import com.paypani.types.exception.InvalidUserException;

/**
 * Implementation for userdao
 */
public class UserDaoImpl implements UserDao {

	private static final Logger LOG = Logger.getLogger(UserDaoImpl.class);

	private EntityManagerFactory emf;

	public String validateUser(User user) throws InvalidCredentialException {
		LOG.debug("inside validateUser method with user >> " + user);
		EntityManager em = getEntityManager();
		Query query = em.createQuery("select c FROM UserEntity c where " + getCriteriaQueryString(user), UserEntity.class);
		List<com.paypani.db.entity.UserEntity> userFromDBList = query.getResultList();
		if (userFromDBList == null || userFromDBList.size() != 1) {
			LOG.debug("No user found with given email or mobile");
			throw new InvalidUserException();
		}

		UserEntity userFromDB = userFromDBList.get(0);
		if (!PasswordHandler.compare(new String(Base64.decode(user.getPassHash())), userFromDB.getPassHash())) {
			LOG.debug("Wrong password entered");
			throw new InvalidPasswordException();
		}
		return JWTTokenGenerator.createJsonWebToken(""+userFromDB.getId(), 100l);
	}

	public boolean exists(String mobile) {
		EntityManager em = getEntityManager();
		Query countQuery = em.createQuery("SELECT COUNT(user.mobile) FROM UserEntity user WHERE user.mobile=:mobile");
		countQuery.setParameter("mobile", mobile);

		Long count = (Long) countQuery.getSingleResult();

		if (count != 0) {
			return true;
		}
		return false;
	}

	public void updatePassword(Long userid, String passhash) throws InvalidUserException, GenericException{
		EntityManager em = getEntityManager();
		try{
			em.getTransaction().begin();
			UserEntity userEntity = em.find(UserEntity.class, userid);
			if(userEntity == null)
				throw new InvalidUserException("unable to find user with userid="+ userid);
			
			userEntity.setPassHash(PasswordHandler.createHash(new String(Base64.decode(passhash)).toCharArray()));
			em.getTransaction().commit();
		}catch(Exception ex){
			LOG.error("Exception updating password " , ex);
			throw new GenericException("unable to update password");
		}
	}

	public void createUser(User user) throws DuplicateAccountException {
		LOG.debug("inside createUser method with user >> " + user);

		if (exists(user.getMobile())) {
			throw new DuplicateAccountException();
		}

		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();

			if (user.getUserDetails() == null) {
				throw new RuntimeException("Can not insert user without details");
			}

			UserEntity userEntity = getEntityFromUser(user);

			userEntity.setPassHash(PasswordHandler.createHash(new String(Base64.decode(user.getPassHash())).toCharArray()));

			em.persist(userEntity);
			em.getTransaction().commit();
		} catch (Exception ex) {
			if (em.getTransaction() != null && em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			LOG.error("unable to create account", ex);
			throw new RuntimeException("Error creatring account with mobile :: " + user.getMobile());
		}
	}

	private User getUserFromEntity(UserEntity userEntity) {

		User user = new User();
		BeanUtil.copyProperties(userEntity, user, null, "com.paypani.types");

		UserDetails userDetails = new UserDetails();
		BeanUtil.copyProperties(userEntity.getUserDetailsEntity(), userDetails, null, "com.paypani.types");

		if (userEntity.getUserDetailsEntity().getAddress() != null) {
			Address address = new Address();
			BeanUtil.copyProperties(userEntity.getUserDetailsEntity().getAddress(), address, null, "com.paypani.types");
			userDetails.setAddress(address);
		}
		user.setUserDetails(userDetails);
		return user;

	}

	private UserEntity getEntityFromUser(User user) {
		UserEntity userEntity = new UserEntity();
		BeanUtil.copyProperties(user, userEntity, null, "com.paypani.db.entity");

		if (user.getUserDetails() == null) {
			throw new RuntimeException("Can not insert user without details");
		}

		UserDetailsEntity userDetails = new UserDetailsEntity();
		BeanUtil.copyProperties(user.getUserDetails(), userDetails, null, "com.paypani.db.entity");

		if (user.getUserDetails().getAddress() != null) {
			AddressEntity addressEntity = new AddressEntity();
			BeanUtil.copyProperties(user.getUserDetails().getAddress(), addressEntity, null, "com.paypani.db.entity");
			userDetails.setAddress(addressEntity);
		}
		userEntity.setUserDetailsEntity(userDetails);
		return userEntity;
	}

	private EntityManager getEntityManager() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("paypani");
		}
		return emf.createEntityManager();
	}

	private String getCriteriaQueryString(User user) {
		StringBuffer queryPart = new StringBuffer();
		if (StringUtils.isNotBlank(user.getEmail())) {
			queryPart.append(" c.name='" + user.getEmail() + "'");
		} else if (StringUtils.isNotBlank(user.getMobile())) {
			queryPart.append(" c.mobile='" + user.getMobile() + "'");
		} else {
			throw new IllegalStateException("Either mobile or email required for login");
		}
		return queryPart.toString();
	}

	@Override
	public User getUserByMobile(String mobile) throws GenericException{
		LOG.debug("inside isUserExist with mobile >> " + mobile);
		EntityManager em = getEntityManager();
		User user = null;
		try {
			Query userQuery = em.createQuery("SELECT user FROM UserEntity user WHERE user.mobile=:mobile", UserEntity.class);
			userQuery.setParameter("mobile", mobile);
			List<UserEntity> usersFromDB = userQuery.getResultList();
			
			if(usersFromDB != null && usersFromDB.size() > 0){
				user = getUserFromEntity(usersFromDB.get(0));
				user.setPassHash("DEMO");
			}
		} catch (Exception ex) {
			LOG.error("Error getting user with mobile = " + mobile, ex);
			throw new GenericException("Error getting user"); 
		}
		return user;
	}

	@Override
	public User getUserByEmail(String email) throws GenericException {
		EntityManager em = getEntityManager();
		User user = null;
		try {
			Query query = em.createQuery("select user FROM UserEntity user where user.email=:email", UserEntity.class);
			query.setParameter("email", email);
			List<UserEntity> usersFromDB = query.getResultList();
			if(usersFromDB != null && usersFromDB.size() > 0){
				user = getUserFromEntity(usersFromDB.get(0));
			}
		} catch (Exception ex) {
			LOG.error("Error getting user with email = " + email, ex);
			throw new GenericException("Error getting user");
		}
		return user;
	}
}
