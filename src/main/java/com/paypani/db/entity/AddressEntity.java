package com.paypani.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "USER_ADDRESS")
@SequenceGenerator(name = "USER_ADDRESS_SEQUENCE", sequenceName = "USER_ADDRESS_SEQUENCE", allocationSize = 1, initialValue = 1)
public class AddressEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_ADDRESS_SEQUENCE")
	private Long id;

	@Column(name = "USER_CITY")
	private String city;

	@Column(name = "ADDRESS_LINE1")
	private String line1;

	@Column(name = "ADDRESS_SECTOR")
	private String sector;

	@Column(name = "USER_ZIP")
	private String zip;

	@Column(name = "USER_STATE")
	private String state;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
