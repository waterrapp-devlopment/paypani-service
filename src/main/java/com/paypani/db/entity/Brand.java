package com.paypani.db.entity;

/**
 *
 */
public class Brand {

    private int id;

    private String name;

    private boolean isISO;

    private boolean isPremium;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isISO() {
        return isISO;
    }

    public void setISO(boolean isISO) {
        this.isISO = isISO;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean isPremium) {
        this.isPremium = isPremium;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("Name :").append(name).append("==")
                                  .append("ISO Certified").append(isISO).append("==")
                                  .append("Premium Brand").append(isPremium).toString();
    }
}
