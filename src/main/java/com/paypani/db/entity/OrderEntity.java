package com.paypani.db.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "USER_ORDER")
@SequenceGenerator(name = "USER_ORDER_SEQUENCE", sequenceName = "USER_ORDER_SEQUENCE", allocationSize = 1, initialValue = 1)
public class OrderEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_ORDER_SEQUENCE")
	@Column(name = "ORDER_ID")
	private Long orderId;

	// TODO need to add this support later
	/*
	 * @OneToOne(optional = false, fetch = FetchType.EAGER)
	 * 
	 * @JoinColumn(name="USER_ID", nullable=false) private UserEntity
	 * userEntity;
	 */

	@Column(name = "USER_MOBILE")
	private String mobile;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ADDRESS_ID")
	private AddressEntity addressEntity;

	@Column(name = "ORDER_QUANTITY")
	private Long quantity;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DELIVERY_DATE")
	private Date deliveredDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "STATUS_ID", nullable = false)
	private OrderStatus orderStatus;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SMS_STATUS_ID", nullable = false)
	private SMSStatus smsStatus;

	@Column(name = "SMS_CORRELATION")
	private String smsCorrealtion;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public AddressEntity getAddressEntity() {
		return addressEntity;
	}

	public void setAddressEntity(AddressEntity addressEntity) {
		this.addressEntity = addressEntity;
	}

	public SMSStatus getSmsStatus() {
		return smsStatus;
	}

	public void setSmsStatus(SMSStatus smsStatus) {
		this.smsStatus = smsStatus;
	}

	public String getSmsCorrealtion() {
		return smsCorrealtion;
	}

	public void setSmsCorrealtion(String smsCorrealtion) {
		this.smsCorrealtion = smsCorrealtion;
	}

}
