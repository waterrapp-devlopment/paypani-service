package com.paypani.db.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="USER_DETAILS")
@SequenceGenerator(name = "USER_DETAILS_SEQUENCE", sequenceName = "USER_DETAILS_SEQUENCE", allocationSize = 1, initialValue = 1)
public class UserDetailsEntity {

	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_DETAILS_SEQUENCE")
	    private Long id;

	    @Column(name="USERNAME")
	    private String username;

	    @Column(name="FIRST_NAME")
	    private String firstName;

	    @Column(name="LAST_NAME")
	    private String lastName;
	    
	    @OneToOne(cascade = CascadeType.PERSIST, optional = false)
	    @JoinColumn(name="USER_ID")
	    private UserEntity UserEntity;

	    @OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER, orphanRemoval = true)
	    @JoinColumn(name="ADDRESS_ID", nullable=false)
	    private AddressEntity address;
	    
	    public UserEntity getUserEntity() {
			return UserEntity;
		}

		public void setUserEntity(UserEntity userEntity) {
			UserEntity = userEntity;
		}

		public AddressEntity getAddress() {
			return address;
		}

		public void setAddress(AddressEntity address) {
			this.address = address;
		}

		public String getFirstName() {
	        return firstName;
	    }

	    public void setFirstName(String firstName) {
	        this.firstName = firstName;
	    }

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getLastName() {
	        return lastName;
	    }

	    public void setLastName(String lastName) {
	        this.lastName = lastName;
	    }

	    public String getUsername() {
	        return username;
	    }

	    public void setUsername(String username) {
	        this.username = username;
	    }
	}
