package com.paypani.db.entity;

import javax.persistence.*;

@Entity
@Table(name = "APP_USER")
@SequenceGenerator(name = "APP_USER_SEQUENCE", sequenceName = "APP_USER_SEQUENCE", allocationSize = 1, initialValue = 1)
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_USER_SEQUENCE")
    private Long id;

    @Column(name="USER_EMAIL")
    private String email;
    
    @Column(name="USER_MOBILE")
    private String mobile;
    
    @Column(name="PASS_HASH")
    private String passHash;
    
    @OneToOne(mappedBy="UserEntity", cascade = CascadeType.PERSIST, optional = false)
    private UserDetailsEntity userDetailsEntity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassHash() {
		return passHash;
	}

	public void setPassHash(String passHash) {
		this.passHash = passHash;
	}

	public UserDetailsEntity getUserDetailsEntity() {
		return userDetailsEntity;
	}

	public void setUserDetailsEntity(UserDetailsEntity userDetailsEntity) {
		this.userDetailsEntity = userDetailsEntity;
	}
	
	

 
}
