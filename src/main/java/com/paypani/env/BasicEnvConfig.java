package com.paypani.env;

import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

/**
 * @author Anuj
 */
public class BasicEnvConfig implements EnvConfig {

	protected BasicEnvConfig(String env) {
		int idx = env.indexOf(',');
		if (idx != -1) {
			env = env.substring(0, idx);
		}
		_envStr = "_" + env;
	}

	public InputStream getEnvResourceAsStream(String resourceName) {
		try {
			// return new FileInputStream(getEnvResource(resourceName).getFile());
			return getEnvResource(resourceName).openStream();
		} catch (Exception e) {
			return null;
		}
	}

	public URL getEnvResource(String resourceName) {

		URL url = BasicEnvConfig.class.getClassLoader().getResource(
				environmentalizeFileResource(resourceName));
		if (url == null){
			url = getClass().getResource(environmentalizeFileResource(resourceName));
		}
		if (url != null){
			return url;
		}
		url = getClass().getResource(resourceName);
		if(url==null){
			url = BasicEnvConfig.class.getClassLoader().getResource(resourceName);
		}
		return url;
	}

	public URL getEnvResourceNoDefault(String resourceName)
	{
	    return BasicEnvConfig.class.getClassLoader().getResource(
	            environmentalizeFileResource(resourceName));
	}

	public String getEnvProperty(String propName, String resourceName) {
		ResourceBundle rb = loadResourceBundle(resourceName);
		return rb.getString(propName);
	}

	public String getEnvProperty(String propName, String resourceName,
			Locale locale) {
		ResourceBundle rb = loadResourceBundle(resourceName, locale);
		return rb.getString(propName);
	}

	public String getEnvString()
	{
		return _envStr;
	}

	protected ResourceBundle loadResourceBundle(String resourceName) {
		try {
			String environmentalizedName = environmentalizePropertiesResource(resourceName);
			LOG.debug("Attempting to load " + environmentalizedName);
			return ResourceBundle.getBundle(environmentalizedName);
		} catch (Exception e) {
			LOG.debug("Did not find environment-specific " + resourceName + ", attempting to load base version");
			return ResourceBundle.getBundle(resourceName);
		}
	}

	protected ResourceBundle loadResourceBundle(String resourceName,
			Locale locale) {
		try {
			return ResourceBundle.getBundle(environmentalizePropertiesResource(resourceName), locale);
		} catch (Exception e) {
			return ResourceBundle.getBundle(resourceName, locale);
		}
	}

	public String environmentalizeFileResource(String fileName) {
		int dot = fileName.lastIndexOf(".");
		return fileName.substring(0, dot) + _envStr + "."
				+ fileName.substring(dot + 1);
	}

	protected String environmentalizePropertiesResource(String propsName) {
		return propsName + _envStr;
	}
	
	public boolean containsEnvProperty(String propName, String resourceName)
	{
		boolean available = false;
		ResourceBundle rb = loadResourceBundle(resourceName);
		for (Enumeration e = rb.getKeys(); e.hasMoreElements();)
		{
			if (((String) e.nextElement()).equals(propName))
			{
				available = true;
				break;
			}
		}
		return available;
	}

	public boolean containsEnvProperty(String propName, String resourceName, Locale locale)
	{
		boolean available = false;
		ResourceBundle rb = loadResourceBundle(resourceName, locale);
		for (Enumeration e = rb.getKeys(); e.hasMoreElements();)
		{
			if (((String) e.nextElement()).equals(propName))
			{
				available = true;
				break;
			}
		}
		return available;
	}

	private String _envStr;

	private static final Logger LOG = Logger.getLogger(BasicEnvConfig.class);

}
