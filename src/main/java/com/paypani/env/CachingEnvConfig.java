package com.paypani.env;

import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * @author Anuj
 *
 * This class is a caching version of <code>BasicEnvConfig</code>.
 */
public class CachingEnvConfig extends BasicEnvConfig  
{

	protected CachingEnvConfig(String env) {
		super(env);
	}
	
	public URL getEnvResource(String resourceName) {
		String cacheKey = toCacheKey(resourceName);
		URL url = (URL) URL_CACHE.get(cacheKey);
		if (url != null) return url;
		url = super.getEnvResource(resourceName);
		URL_CACHE.put(cacheKey, url);
		return url;
	}
	
	protected ResourceBundle loadResourceBundle(String resourceName) {
		String cacheKey = toCacheKey(resourceName);
		ResourceBundle rb = (ResourceBundle) PROPS_CACHE.get(cacheKey);
		if (rb != null) return rb;
		rb = super.loadResourceBundle(resourceName);
		PROPS_CACHE.put(cacheKey, rb);
		return rb;
	}
	
	protected ResourceBundle loadResourceBundle(String resourceName, Locale locale) {
		String cacheKey = toCacheKey(resourceName, locale);
		ResourceBundle rb = (ResourceBundle) PROPS_CACHE.get(cacheKey);
		if (rb != null) return rb;
		rb = super.loadResourceBundle(resourceName, locale);
		PROPS_CACHE.put(cacheKey, rb);
		return rb;
	}
	
	protected String toCacheKey(String resourceName) {
		return resourceName;
	}
	
	protected String toCacheKey(String resourceName, Locale locale) {
		return resourceName + locale.getCountry() + locale.getLanguage() + locale.getVariant();
	}

   
	protected static final Map PROPS_CACHE = new HashMap();
	protected static final Map URL_CACHE = new HashMap();
}