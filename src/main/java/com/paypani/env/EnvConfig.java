package com.paypani.env;

import java.io.InputStream;
import java.net.URL;
import java.util.Locale;
import java.util.Properties;

/**
 * In order to load properties based on environment this tier will look in the
 * root of the classpath for the file env.properties which should specify a 
 * single property "env".  Example: env=dev.
 * Based on the value of that property, EnvConfigs will append .dev to any file
 * that is loaded, allowing developers to check in multiple versions of the 
 * file at the same time that will transparently work as the code is promoted
 * from environment to environment.  Example: media.properties.dev, 
 * media.properties.qa, media.properties.beta, media.properties.prod.
 * 
 * @author Anuj
 */
public interface EnvConfig {

	public static class FACTORY {
		private static Properties env;
		static {
			try {
				env = new Properties();
				if (null != System.getProperty("env")) {
					env.setProperty("env", System.getProperty("env"));
				}
				else {
					env.load(
						EnvConfig.FACTORY.class.getClassLoader().getResourceAsStream("env.properties"));
				}
			} catch (Exception e) {
				throw new RuntimeException(
						"EnvConfig.FACTORY.static - Unable to load env.properties! " + e.getMessage());
			}
        }

         public static EnvConfig getEnvConfig(){
        	 return new CachingEnvConfig(ENV);
        }

        private static EnvConfig getBasicEnvConfig(String env){
              return new BasicEnvConfig(env);
       }

        private static EnvConfig getCachedEnvConfig(String env){
             return new CachingEnvConfig(env);
       }

		private static final String ENV = env.getProperty("env");
		
		// no need to have users create a bunch of instances, one is enough
		public static final EnvConfig BASIC = getBasicEnvConfig(ENV);
		public static final EnvConfig CACHED = getCachedEnvConfig(ENV);
	}
	
	public URL getEnvResource(String resourceName);
    
    public URL getEnvResourceNoDefault(String resourceName);
	
	public InputStream getEnvResourceAsStream(String resourceName);
    
    public String getEnvProperty(String propName, String resourceName);
    
    public String getEnvProperty(String propName, String resourceName, Locale locale);
    
    public String getEnvString();
    
    public boolean containsEnvProperty(String propName, String resourceName);
    
    public boolean containsEnvProperty(String propName, String resourceName, Locale locale);
    
    public String environmentalizeFileResource(String fileName);

    
}
