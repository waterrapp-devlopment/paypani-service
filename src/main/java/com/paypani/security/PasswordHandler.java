package com.paypani.security;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import com.paypani.security.util.Base64;


public class PasswordHandler {
    public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";

    public static final int SALT_BYTE_SIZE = 16;
    public static final int HASH_BYTE_SIZE = 16;
    public static final int PBKDF2_5000_ITERATIONS = 5000;
    public static final int PBKDF2_3000_ITERATIONS = 3000;

    public static final int SALT_INDEX = 0;
    public static final int PBKDF2_INDEX = 1;

    public PasswordHandler() {

    }

    public static boolean compare(String unHashed, String hashed) {
        if (unHashed == null || hashed == null) {
            return false;
        }

        String[] params = hashed.split(":");
        String salt = params[SALT_INDEX];
        String hash = params[PBKDF2_INDEX];

        try {
            if ((validatePassword(unHashed, hash, salt, PBKDF2_5000_ITERATIONS)) || (validatePassword(unHashed, hash, salt, PBKDF2_3000_ITERATIONS))) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw new RuntimeException("Error comparing passwords with PBKDF2", e);
        }
    }

    public String hash(String unHashed) {
        try {
            return createHash(unHashed.toCharArray());
        } catch (Exception ex) {
            throw new RuntimeException("Error hashing password:" + unHashed, ex);
        }
    }

    public static String createHash(char[] password) throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException {
        // Generate a random salt
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[SALT_BYTE_SIZE];
        random.nextBytes(salt);
        // Hash the password
        byte[] hash = pbkdf2(password, salt, PBKDF2_5000_ITERATIONS, HASH_BYTE_SIZE);
        /* delimiter for hash and salt is ':' */
        return Base64.encodeToString(salt, false) + ":" + Base64.encodeToString(hash, false);
    }

    // compare if two passwords are equal
    private static boolean slowEquals(byte[] a, byte[] b) {
        int diff = a.length ^ b.length;
        for (int i = 0; i < a.length && i < b.length; i++)
            diff |= a[i] ^ b[i];
        return diff == 0;
    }

    private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes) throws NoSuchAlgorithmException, InvalidKeySpecException {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
        return skf.generateSecret(spec).getEncoded();
    }

    public static boolean validatePassword(String password, String correctHash, String salt, int iterations) throws NoSuchAlgorithmException,
            InvalidKeySpecException, UnsupportedEncodingException {
        byte[] saltBytes = Base64.decode(salt);
        byte[] correctHashBytes = Base64.decode(correctHash);
        byte[] testHashBytes = pbkdf2(password.toCharArray(), saltBytes, iterations, HASH_BYTE_SIZE);
        return slowEquals(correctHashBytes, testHashBytes);
    }
    
    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException {
    	String pass = Base64.encodeToString("password".getBytes("UTF-8"));
    	//System.out.println(Base64.encodeToString("password".getBytes("UTF-8")));
		System.out.println(compare(new String(Base64.decode(pass)), "80Jv3bkV/vvhP4/xgu9cZw==:ig5fylXCJJ6DwEbeoBzxiQ=="));
	}
}

