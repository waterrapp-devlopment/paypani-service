package com.paypani.security.util;

import java.util.Arrays;

public class Base64 {

    private static final char[] CHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
    private static final int[] IARR = new int[256];

    static {
        Arrays.fill(IARR, -1);
        for (int i = 0, iS = CHA.length; i < iS; i++)
            IARR[CHA[i]] = i;
        IARR['='] = 0;
    }
    
    public static boolean isValid(String encodedString){
    	boolean valid = true;
    	try{
    		decode(encodedString);
    	}catch(Exception ex){
    		valid = false;
    	}
    	return valid;
    }
    
    public static void main(String[] args) {
		System.out.println(new String(encodeToString("password".getBytes())));
	}

    public final static char[] encodeToChar(byte[] sArr, boolean lineSep) {
        int sLen = sArr != null ? sArr.length : 0;
        if (sLen == 0)
            return new char[0];

        int eLen = (sLen / 3) * 3;
        int chCnt = ((sLen - 1) / 3 + 1) << 2;
        int dLen = chCnt + (lineSep ? (chCnt - 1) / 76 << 1 : 0);
        char[] dArr = new char[dLen];

        for (int s = 0, d = 0, cc = 0; s < eLen; ) {
            int i = (sArr[s++] & 0xff) << 16 | (sArr[s++] & 0xff) << 8 | (sArr[s++] & 0xff);

            dArr[d++] = CHA[(i >>> 18) & 0x3f];
            dArr[d++] = CHA[(i >>> 12) & 0x3f];
            dArr[d++] = CHA[(i >>> 6) & 0x3f];
            dArr[d++] = CHA[i & 0x3f];

            if (lineSep && ++cc == 19 && d < dLen - 2) {
                dArr[d++] = '\r';
                dArr[d++] = '\n';
                cc = 0;
            }
        }

        int left = sLen - eLen; // 0 - 2.
        if (left > 0) {
            int i = ((sArr[eLen] & 0xff) << 10) | (left == 2 ? ((sArr[sLen - 1] & 0xff) << 2) : 0);

            dArr[dLen - 4] = CHA[i >> 12];
            dArr[dLen - 3] = CHA[(i >>> 6) & 0x3f];
            dArr[dLen - 2] = left == 2 ? CHA[i & 0x3f] : '=';
            dArr[dLen - 1] = '=';
        }
        return dArr;
    }

    public final static String encodeToString(byte[] sArr, boolean lineSep) {
        return new String(encodeToChar(sArr, lineSep));
    }

    //encodes a string with no line separator
    public final static String encodeToString(byte[] sArr) {
        return new String(encodeToChar(sArr, false));
    }

    public final static byte[] decode(String str) {
        int strLen = str != null ? str.length() : 0;
        if (strLen == 0)
            return new byte[0];

        int sepCnt = 0;
        for (int i = 0; i < strLen; i++)
            if (IARR[str.charAt(i)] < 0)
                sepCnt++;

        if ((strLen - sepCnt) % 4 != 0)
            throw new IllegalArgumentException("Unable to decode Base64 string " + str);

        int pad = 0;
        for (int i = strLen; i > 1 && IARR[str.charAt(--i)] <= 0; )
            if (str.charAt(i) == '=')
                pad++;

        int len = ((strLen - sepCnt) * 6 >> 3) - pad;

        byte[] deArr = new byte[len];

        for (int s = 0, d = 0; d < len; ) {
            int i = 0;
            for (int j = 0; j < 4; j++) {
                int c = IARR[str.charAt(s++)];
                if (c >= 0)
                    i |= c << (18 - j * 6);
                else
                    j--;
            }
            deArr[d++] = (byte) (i >> 16);
            if (d < len) {
                deArr[d++] = (byte) (i >> 8);
                if (d < len)
                    deArr[d++] = (byte) i;
            }
        }
        return deArr;
    }
}
