package com.paypani.service;

import java.util.List;

import com.paypani.db.dao.OrderDao;
import com.paypani.db.dao.impl.OrderDaoImpl;
import com.paypani.types.Order;
import com.paypani.types.exception.GenericException;

public class OrderService {
	
	public Long createOrder(Order order){
		OrderDao orderDao = new OrderDaoImpl();
		return orderDao.createOrder(order);
	}
	
	public List<Order> findAllOrders() throws GenericException{
		OrderDao orderDao = new OrderDaoImpl();
		return orderDao.getAllOrders();
	}
	
	public void updateSmsCorrelationId(Long orderId, String correlationId){
		OrderDao orderDao = new OrderDaoImpl();
		orderDao.updateSmsCorrelationId(orderId, correlationId);
	}

}
