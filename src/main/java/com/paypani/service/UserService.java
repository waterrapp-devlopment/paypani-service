package com.paypani.service;

import com.paypani.db.dao.UserDao;
import com.paypani.db.dao.impl.UserDaoImpl;
import com.paypani.types.User;
import com.paypani.types.exception.DuplicateAccountException;
import com.paypani.types.exception.GenericException;
import com.paypani.types.exception.InvalidCredentialException;
import com.paypani.types.exception.InvalidUserException;

/**
 *
 */
public class UserService {

    UserDao userDao = new UserDaoImpl();

    public String validateUser(User user) throws InvalidCredentialException{
        return userDao.validateUser(user);
    }
    
    public void createUser(User user) throws DuplicateAccountException{
        userDao.createUser(user);
    }
    
    public User getUserByMobile(String mobile) throws GenericException{
       return userDao.getUserByMobile(mobile);
    }
    
    public User getUserByEmail(String email) throws GenericException{
       return userDao.getUserByEmail(email);
    }
    
    public void updatePassword(Long userid, String passhash) throws InvalidUserException, GenericException{
    	 userDao.updatePassword(userid, passhash);
    }
}
