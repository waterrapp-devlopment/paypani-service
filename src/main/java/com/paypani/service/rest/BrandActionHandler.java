package com.paypani.service.rest;

import com.paypani.db.entity.UserEntity;
import com.paypani.types.response.BrandServiceResponse;
import com.sun.jersey.api.core.InjectParam;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.ResponseBuilder;

@Path("/brand")
public class BrandActionHandler {

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(@InjectParam UserEntity user){
        return Response.status(marshalServiceResponseToJersey(new BrandServiceResponse(1l, "success"))).build();
    }

    public Status marshalServiceResponseToJersey(BrandServiceResponse brandServiceResponse){
        if(BrandServiceResponse.SUCCESS_STATUS_CODE.equals(brandServiceResponse.getStatusCode())){
            return Status.OK;
        } else {
          return  Status.INTERNAL_SERVER_ERROR;
        }
    }
}
