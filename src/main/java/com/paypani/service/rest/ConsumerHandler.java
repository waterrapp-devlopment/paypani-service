package com.paypani.service.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.paypani.amqp.ConnectionFactory;
import com.paypani.service.sms.task.SMSConsumerTask;
import com.rabbitmq.client.Channel;


@Path("/consumer")
public class ConsumerHandler {
	
	private static final Map<String, Thread> threadMap = new HashMap<String, Thread>();
	
	@Path("/startnew")
	@GET
	public Response start(@QueryParam("queueName") String queueName){
		String consumerName = UUID.randomUUID().toString();
		
		Channel channel = ConnectionFactory.getInstance(queueName).getChannel();
		SMSConsumerTask consumerTask = new SMSConsumerTask(queueName, channel, consumerName);
		
		Thread thread = new Thread(consumerTask);
		threadMap.put(consumerName, thread);
		thread.start();
		
		return Response.ok("Successfully started thread with id :: " + consumerName).build();
	}
	
	@Path("/stop")
	@GET
	public Response stop(@QueryParam("consumerid") String consumerId){
		String status = "";
		Thread thread = threadMap.get(consumerId);
		if(thread != null){
			thread.stop();
			threadMap.remove(consumerId);
			status = "successfulle stopped thread with id" +  consumerId;
		}else{
			status = "No consumer running with id " + consumerId;
		}
		return Response.ok(status).build();
	}

}
