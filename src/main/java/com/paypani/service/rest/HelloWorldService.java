package com.paypani.service.rest;

import com.paypani.db.core.ConnectionManager;
import com.paypani.db.core.DataSource;
import com.paypani.db.entity.UserEntity;

import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.crypto.Data;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Path("/hello")
public class HelloWorldService {

    @GET
    @Path("/{param}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMsg(@PathParam("param") String msg) throws PropertyVetoException, SQLException, IOException {

        String output = "Jersey say : " + msg;


        return Response.status(200).entity(output).build();

    }

}