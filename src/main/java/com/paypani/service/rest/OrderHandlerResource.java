package com.paypani.service.rest;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mysql.fabric.xmlrpc.base.Data;
import com.paypani.service.OrderService;
import com.paypani.service.sms.SMSPusher;
import com.paypani.service.sms.SMSService;
import com.paypani.service.sms.SMSServiceImpl;
import com.paypani.service.sms.types.SMSContext;
import com.paypani.service.sms.types.SimpleSMSSender;
import com.paypani.service.util.DateFormatter;
import com.paypani.types.Order;
import com.paypani.types.exception.InvalidParameterException;
import com.paypani.types.response.FailedServiceResponse;
import com.paypani.types.response.ServiceResponse;
import com.paypani.types.response.SuccessServiceResponse;
import com.paypani.validator.OrderValidator;
import com.paypani.validator.Validator;

@Path("/order")
public class OrderHandlerResource{
	
	private static final Logger log = Logger.getLogger(OrderHandlerResource.class);
	
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ServiceResponse create(Order order){
		log.debug("inside create with order " + order);
		ServiceResponse serviceResponse = new SuccessServiceResponse();
		OrderService orderService = new OrderService();
		try{
			Validator validator = new OrderValidator();
			validator.validate(order);
			
			Long orderId = orderService.createOrder(order);
			if(orderId == null){
				throw new InvalidParameterException("Unable to create order");
			}
			order.setOrderId(orderId);
		
			//send order data to sms queue
			SMSPusher.sendToQueue(order);
			
			serviceResponse.getData().put("orderId", orderId);
			serviceResponse.getData().put("orderTime", ""+ DateFormatter.toDateString(new Date()));
			serviceResponse.getData().put("quantity", order.getQuantity());
			serviceResponse.getData().put("address", order.getDeliveryAddress());
		}catch(InvalidParameterException ipe){
			log.error("Error creating order", ipe);
			serviceResponse = new FailedServiceResponse(ServiceResponse.CODE_INVALID_REQUEST_ERROR, ServiceResponse.MSG_INVALID_REQUEST_ERROR);
		}catch(Exception ex){
			log.error("Error creating order", ex);
			serviceResponse = new FailedServiceResponse(ServiceResponse.CODE_GENERIC_ERROR, ServiceResponse.MSG_GENERIC_ERROR);
		}
		
		return serviceResponse;
	}
	
	@POST
	@Path("/sendsms")
	public Response sendSMS(String message){
		SimpleSMSSender sender = new SimpleSMSSender();
		
		SMSContext context = new SMSContext();
		context.setSmsBody(message);
		sender.send(context);
		return Response.ok("Worked!! yeppe").build();
	}
	
	private String createSMSBody(Order order){
		StringBuilder sb = new StringBuilder();
		sb.append("Your order placed successfully with order number");
		sb.append(" " +  order.getOrderId());
		return sb.toString();
	}
	
	@GET
	@Path("/findall")
	@Produces(MediaType.APPLICATION_JSON)
	public ServiceResponse findAllOrders(){
		ServiceResponse serviceResponse = new SuccessServiceResponse();
		OrderService orderService = new OrderService();
		try{
			serviceResponse.getData().put("orders", orderService.findAllOrders());
		}catch(Exception ex){
			serviceResponse = new FailedServiceResponse(ServiceResponse.CODE_GENERIC_ERROR, ServiceResponse.MSG_GENERIC_ERROR);
		}
		return serviceResponse;
	}

}
