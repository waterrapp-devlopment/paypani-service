package com.paypani.service.rest;

import com.paypani.security.util.Base64;
import com.paypani.service.UserService;
import com.paypani.types.User;
import com.paypani.types.exception.DuplicateAccountException;
import com.paypani.types.exception.GenericException;
import com.paypani.types.exception.InvalidCredentialException;
import com.paypani.types.exception.InvalidUserException;
import com.paypani.types.response.FailedServiceResponse;
import com.paypani.types.response.ServiceResponse;
import com.paypani.types.response.SuccessServiceResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * User Account Handling
 */
@Path("/user")
public class UserHandlerResource {

	private static final Logger LOG = Logger.getLogger(UserHandlerResource.class);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/validate")
	public ServiceResponse validate(@QueryParam("mobile") String mobile, @QueryParam("email") String email, @QueryParam("passhash") String passHash)
			throws Exception {
		LOG.debug("inside validate >> mobile==" + mobile + ", email==" + email + ", passhash==" + passHash);
		ServiceResponse response = new SuccessServiceResponse();

		try {
			if (StringUtils.isBlank(mobile) && StringUtils.isBlank(email)) {
				throw new InvalidCredentialException("Missing required parameter >>> email or mobile");
			}

			if (StringUtils.isBlank(passHash) || !Base64.isValid(passHash)) {
				throw new InvalidCredentialException("Missing or Invalid passHash");
			}
			User user = new User();
			user.setEmail(email);
			user.setMobile(mobile);
			user.setPassHash(passHash);

			UserService userService = new UserService();
			String accessToken = userService.validateUser(user);

			// if successful
			response.getData().put("accessToken", accessToken);
		} catch (InvalidCredentialException ice) {
			LOG.error("Invalid credential entered ", ice);
			response = new FailedServiceResponse();
		} catch (Exception ex) {
			LOG.error("Error validating", ex);
			response = generateFailedResponse(ServiceResponse.CODE_GENERIC_ERROR, ServiceResponse.MSG_GENERIC_ERROR);
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/find")
	public ServiceResponse findUser(@QueryParam("email") String email, @QueryParam("mobile") String mobile) {
		ServiceResponse response = new SuccessServiceResponse();
		try {
			User user = null;
			UserService userService = new UserService();

			if (StringUtils.isNotBlank(email)) {
				user = userService.getUserByEmail(email);
			}

			if (user == null && StringUtils.isNotBlank(mobile)) {
				user = userService.getUserByMobile(mobile);
			}

			if (user == null) {
				throw new InvalidUserException("No user found with mobile " + mobile + " or email = " + email);
			}
			user.setPassHash("DEMO");
			response.getData().put("user", user);

		} catch (InvalidUserException ex) {
			response = generateFailedResponse(ServiceResponse.CODE_INVALID_USER_ERROR, ServiceResponse.MSG_INVALID_USER_ERROR);
		} catch (Exception ex) {
			response = generateFailedResponse(ServiceResponse.CODE_GENERIC_ERROR, ServiceResponse.MSG_GENERIC_ERROR);
		}
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updatepass")
	public ServiceResponse updatepassword(@QueryParam("passhash") String passhash, @QueryParam("id") Long userId) {
		ServiceResponse response = new SuccessServiceResponse();
		try {
			UserService userService = new UserService();
			userService.updatePassword(userId, passhash);
		} catch (InvalidUserException ex) {
			response = generateFailedResponse(ServiceResponse.CODE_INVALID_USER_ERROR, ServiceResponse.MSG_INVALID_USER_ERROR);
		} catch (Exception ex) {
			response = generateFailedResponse(ServiceResponse.CODE_GENERIC_ERROR, ServiceResponse.MSG_GENERIC_ERROR);
		}
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/register")
	public ServiceResponse register(String userData) {
		ServiceResponse response = new SuccessServiceResponse();
		try {
			if (StringUtils.isBlank(userData)) {
				throw new RuntimeException("No request data");
			}
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			User user = mapper.readValue(userData, User.class);

			UserService userService = new UserService();
			userService.createUser(user);
			response.getData().put("user", user);
		} catch (DuplicateAccountException dae) {
			LOG.error("Duplicate Account Error", dae);
			response = generateFailedResponse(ServiceResponse.CODE_DUPLICATE_ERROR, ServiceResponse.MSG_DUPLICATE_ERROR);
		} catch (Exception jse) {
			LOG.error("Error in registering user", jse);
			response = generateFailedResponse(ServiceResponse.CODE_GENERIC_ERROR, ServiceResponse.MSG_GENERIC_ERROR);
		}
		return response;
	}

	private ServiceResponse generateFailedResponse(String code, String message) {
		FailedServiceResponse response = new FailedServiceResponse();
		response.setCode(code);
		response.setMessage(message);
		return response;
	}
}
