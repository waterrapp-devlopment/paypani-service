package com.paypani.service.sms;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.paypani.amqp.ConnectionFactory;
import com.paypani.amqp.ConnectionFactoryPool;
import com.paypani.amqp.ConnectionObjectPool;
import com.paypani.types.Order;

public class SMSPusher {
	
	private static final String QUEUE_NAME = "paypaani.sms.incoming.queue";
	
	private static final Logger log = Logger.getLogger(SMSPusher.class);
	
	private static ConnectionObjectPool pool = new ConnectionObjectPool(new ConnectionFactoryPool(QUEUE_NAME));
	
	public static void sendToQueue(Order order) throws Exception{
		ConnectionFactory connection = null;
		try{
			connection = (ConnectionFactory) pool.borrowObject();
			connection.publishDataToQueue("", QUEUE_NAME, null, getJsonFromObject(order).getBytes());
		}catch(Exception ex){
			log.error("Error pushing to sms queue", ex);
		}finally{
			if(connection != null){
				pool.returnObject(connection);
			}
		}
		
	}
	
	private static String getJsonFromObject(Order order){
		Gson gson = new Gson();
		return gson.toJson(order);
	}

}
