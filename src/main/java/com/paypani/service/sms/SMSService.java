package com.paypani.service.sms;

import com.paypani.service.sms.types.SMSContext;

public interface SMSService {
	
	public String sendSMS(SMSContext context);

}
