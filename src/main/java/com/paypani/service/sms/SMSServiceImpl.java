package com.paypani.service.sms;

import com.paypani.service.sms.types.SMSContext;
import com.paypani.service.sms.types.SMSSender;

public class SMSServiceImpl implements SMSService{
	
	private SMSSender sender;
	
	public SMSServiceImpl(SMSSender sender){
		this.sender = sender;
	}
	
	@Override
	public String sendSMS(SMSContext context) {
		if(sender == null){
			throw new RuntimeException("Sender can not be null");
		}
		
		return sender.send(context);
	}
}
