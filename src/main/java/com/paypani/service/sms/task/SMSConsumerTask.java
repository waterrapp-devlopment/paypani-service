package com.paypani.service.sms.task;

import org.apache.log4j.Logger;

import com.rabbitmq.client.Channel;

public class SMSConsumerTask implements Runnable{
	
	private String queueName;
	
	private Channel channel;
	
	private String consumerId;
	
	private static final Logger log = Logger.getLogger(SMSConsumerTask.class);
	
	public SMSConsumerTask(String queueName, Channel channel, String id){
		this.channel = channel;
		this.queueName = queueName;
        this.consumerId = id;
	}

	@Override
	public void run() {
		try{
			SMSQueueConsumer consumer = new SMSQueueConsumer(channel, consumerId);
			log.debug("starting consumer");
			channel.basicConsume(queueName, true, consumer);
		}catch(Exception ex){
			log.error("Exception in  consumer", ex);
		}
	}
}
