package com.paypani.service.sms.task;

import java.io.IOException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.paypani.service.OrderService;
import com.paypani.service.sms.SMSService;
import com.paypani.service.sms.SMSServiceImpl;
import com.paypani.service.sms.types.SMSContext;
import com.paypani.service.sms.types.SMSSender;
import com.paypani.service.sms.types.SimpleSMSSender;
import com.paypani.service.util.DateFormatter;
import com.paypani.types.Order;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class SMSQueueConsumer extends DefaultConsumer {

	private String id;
	
	private static final Logger log = Logger.getLogger(SMSQueueConsumer.class);
	
	public SMSQueueConsumer(Channel channel, String id) {
		super(channel);
		this.id = id;
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties, byte[] body) throws IOException {
		String message = new String(body, "UTF-8");
		
		try{
			Order order = new Gson().fromJson(message, Order.class);
			
			SMSContext smsContext = new SMSContext();
			smsContext.setMobileNo(order.getMobile());
			smsContext.setSmsBody(getSMSText(order));
			
			SMSSender sender = new SimpleSMSSender();
			SMSService smsService = new SMSServiceImpl(sender);
			String correlationId = smsService.sendSMS(smsContext);
			
			if(correlationId != null){
				OrderService orderService = new OrderService();
				orderService.updateSmsCorrelationId(order.getOrderId(), correlationId);
			}else{
				log.warn("Unable to get correaltion id from sms service");
			}
		}catch(Exception ex){
			log.error("Unavble to handle message " + message, ex);
		}

	}
	
	private String getSMSText(Order order){
		return "Order placed at " + DateFormatter.toDateString(new Date()) + "! with order number " + order.getOrderId() + ". Call 9654003234 for any further assitance ";
	} 

}
