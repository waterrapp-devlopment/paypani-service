package com.paypani.service.sms.types;

public class SMSContext {

	private String mobileNo;

	private String smsBody;

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getSmsBody() {
		return smsBody;
	}

	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}

	@Override
	public String toString() {
		return "SMSContext [mobileNo=" + mobileNo + ", smsBody=" + smsBody + "]";
	}
}
