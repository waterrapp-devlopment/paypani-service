package com.paypani.service.sms.types;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class SMSReceiver {

	private final static String QUEUE_NAME = "paypaani.sms.incoming.queue";

	private static final Logger LOG = Logger.getLogger(SMSReceiver.class);

	public void receive() {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("owl.rmq.cloudamqp.com");
		factory.setUsername("bcohhnli");
		factory.setPassword("lmOoH3O7BnF_kOUXP_IUXmSD9vabC3aU");
		factory.setVirtualHost("bcohhnli");
		Connection connection = null;
		Channel channel = null;
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(QUEUE_NAME, true, false, false, null);
			
			final Consumer consumer = new DefaultConsumer(channel) {
				  @Override
				  public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
				    String message = new String(body, "UTF-8");
				    try {
				    	JSONObject payloadObject = new JSONObject(message);
				      System.out.println("Message received >> Payload" + message);
				    } catch(Exception ex){
				    	LOG.error("Error handling message delivery");
				    }finally {
				      System.out.println(" [x] Done");
				    }
				  }
				};
			channel.basicConsume(QUEUE_NAME, true, consumer);
		} catch (Exception ex) {
			System.err.println(ex);
			LOG.error("Unable to deliver sms to queue", ex);
		}
	}
}
