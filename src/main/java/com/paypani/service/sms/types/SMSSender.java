package com.paypani.service.sms.types;

public interface SMSSender {
	
	public String send(SMSContext context);

}
