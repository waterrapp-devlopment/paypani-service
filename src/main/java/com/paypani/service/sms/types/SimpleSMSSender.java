package com.paypani.service.sms.types;

import java.io.InputStream;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EncodingUtils;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.paypani.service.OrderService;
import com.rabbitmq.client.ConnectionFactory;

public class SimpleSMSSender implements SMSSender {
	
	private static final String propertyFileName = "smsapi.properties";

	private static final Logger LOG = Logger.getLogger(SimpleSMSSender.class);
	
	private static final String api_user;
	
	private static final String api_password;
	
	private static final String api_url;
	
	static{
		try{
			Properties props = new Properties();
			InputStream inputStream = ConnectionFactory.class.getClassLoader().getResourceAsStream(propertyFileName);
			if(inputStream == null)
				throw new RuntimeException("Can not find rabbitmq property file");
			
			props.load(inputStream);
			api_user = props.getProperty("api_user");
			api_password = props.getProperty("api_password");
			api_url = props.getProperty("api_url");
			LOG.debug("found api details[api_user= " + api_user +  ", api_password=" + api_password + ", api_url="+ api_url);
		}catch(Exception ex){
			throw new ExceptionInInitializerError(ex.getMessage());
		}
	}
	

	@Override
	public String send(SMSContext context) {
		LOG.debug("inside send method with " + context);
		String correlationId = null;
		HttpClient client = new DefaultHttpClient();
		try {
			String urlToHit = MessageFormat.format(api_url, new String[]{context.getMobileNo(),  URLEncoder.encode(context.getSmsBody(), "UTF-8")});
			HttpGet request = new HttpGet(urlToHit);
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			String responseStr = EntityUtils.toString(entity);
			String[] responses = responseStr.split("-");
		   correlationId = responses[1];
		} catch (Exception ex) {
			LOG.error("unable to send request to sms api", ex);
		}
		return correlationId;
	}
	
	public static void main(String[] args) {
		SimpleSMSSender sender = new SimpleSMSSender();
		sender.send(new SMSContext());
	}

}
