package com.paypani.service.util;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.paypani.db.entity.AddressEntity;
import com.paypani.types.Address;
import com.paypani.types.User;
import com.paypani.types.UserDetails;

public class BeanUtil {
	
		private static final Logger LOG = Logger.getLogger(BeanUtil.class);

		public static void copyProperties(Object source, Object target, String commaseparatedIgnoreRecursiveProperties, String userPackageForChildVOs){
			
			Class actualEditableClass = target.getClass();
			Class sourceClass = source.getClass();
			
			String sourcePackage = sourceClass.getName().substring(0, sourceClass.getName().lastIndexOf("."));
			try{
				Method[] methods = sourceClass.getDeclaredMethods();
				if(methods!=null && methods.length>0){
					
					for(Method method : methods){
						
						//do not consider any static, private and inherited methods
						if(method.getName().startsWith("get") && Modifier.isPublic(method.getModifiers()) && !(Modifier.isStatic(method.getModifiers()))
								&& !(!isNull(commaseparatedIgnoreRecursiveProperties) && commaseparatedIgnoreRecursiveProperties.contains(method.getName().substring(3)))){
							
							Class returnTypeInterface = null;
							if(method.getReturnType().getInterfaces()!=null && method.getReturnType().getInterfaces().length>0){
								
								for(Class parentInterface : method.getReturnType().getInterfaces()){
									
									if(parentInterface.getName().startsWith("java.util")){
										returnTypeInterface = parentInterface;
									}
								}
							}
							
							//cases of collection and maps to be handled separately.
							if(!(returnTypeInterface!=null
									&& (returnTypeInterface.equals(Collection.class) || returnTypeInterface.equals(Map.class)))){
								
								String targetSetMethodName = "set" + method.getName().substring(3);
								Object value = sourceClass.getMethod(method.getName(), new Class[] {}).invoke(source, new Object[]{});
								
								
								if(value !=null){
									try{
										Method targetMethod = actualEditableClass.getMethod(targetSetMethodName, new Class[] {value.getClass()});
										targetMethod.invoke(target, new Object[]{value});
									}catch (Exception e) {
										//ingnore any exception for target object, as some property may bypass all of the above checks
										
										try{
											Class _instance =  Class.forName(userPackageForChildVOs+"."+value.getClass().getName().substring(value.getClass().getName().lastIndexOf(".")+1));
											Object newTarget = _instance.newInstance();
											actualEditableClass.getMethod(targetSetMethodName, new Class[] {_instance}).invoke(target, new Object[]{newTarget});
											copyProperties(value, newTarget,  commaseparatedIgnoreRecursiveProperties,  userPackageForChildVOs);
										
										}catch (Exception e1) {
											
											//LOG.error(e1,e1);//ignore any exception on target object, as some properties may not be avialable for it.
										}
									}
								}
							}
							
							else{
								
								if(method.getReturnType().getInterfaces()[0].equals(Collection.class)){
									
								//	String targetSetMethodName = "set" + method.getName().substring(3);
									Collection value = (Collection)sourceClass.getMethod(method.getName(), new Class[] {}).invoke(source, new Object[]{});
									
									if(value!=null && value.size()>0){
										Class pInterface = null;
										if(value.getClass().getInterfaces()!=null && value.getClass().getInterfaces().length>0){
											
											for(Class parentInterface : value.getClass().getInterfaces()){
												
												if(parentInterface.getName().startsWith("java.util")){
													pInterface = parentInterface;
												}
											}
										}
										
										
										String targetSetMethodName = "set" + method.getName().substring(3);
										Collection newCollection  = getNewCollectionInstance(pInterface);
										actualEditableClass.getMethod(targetSetMethodName, new Class[] {pInterface}).invoke(target, new Object[]{newCollection});
										
										Iterator itr= value.iterator();
										while(itr.hasNext()){
											
											Class _instance =  Class.forName(userPackageForChildVOs+"."+method.getName().substring(3,method.getName().length()-1));
											Object newTarget = _instance.newInstance();
											newCollection.add(newTarget);
											
											copyProperties(itr.next(), newTarget,  commaseparatedIgnoreRecursiveProperties,  userPackageForChildVOs);
										}
										
										
									}
								}
							}
							
						}
						
					}
				}
			
			}catch (Exception e) {
				LOG.error(e,e);
			}
		}
		
		/**
		 * Determines if the given string has no value.
		 * @param input The String to check.
		 * @return boolean 
		 */
	    private static boolean isNull(String input) {
	    		return (input==null || input.trim().length()<1 || input.equalsIgnoreCase("null"));
	    }
		
		private static Collection getNewCollectionInstance(Class pInterface){
			
			if(pInterface.getClass().getName().equals("java.util.Set")){
				return new HashSet();
			}
			else
				return new HashSet();
		}
		
		public static void main(String[] args) {
			User user = new User();
			user.setEmail("anujgoel");
			user.setMobile("9654003234");
			user.setPassHash("passhash");
			
			UserDetails details = new UserDetails();
			details.setFirstName("firstname");
			details.setLastName("lastname");
			details.setUsername("username");
			
			Address address = new Address();
			address.setCity("city");
			address.setLine1("line");
			address.setSector("sector");
			address.setState("satater");
			address.setZip("zip");
			
			details.setAddress(address);
			
			user.setUserDetails(details);
			
			AddressEntity userEntity = new AddressEntity();
			
			copyProperties(address, userEntity, null, "com.paypani.db.entity");
			System.out.println(userEntity);
		
		}
		
		
	}

