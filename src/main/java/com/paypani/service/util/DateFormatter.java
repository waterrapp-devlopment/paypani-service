package com.paypani.service.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateFormatter {
	
	public static String toDateString(Date date){
		if(date == null)
			return "";
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		return format.format(date);
	}
}
