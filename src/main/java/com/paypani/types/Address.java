package com.paypani.types;

/**
 * User Address
 */
public class Address {

    private String city;

    private String line1;

    private String sector;

    private String zip;

    private String state;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

	@Override
	public String toString() {
		return "Address [city=" + city + ", line1=" + line1 + ", sector=" + sector + ", zip=" + zip + ", state=" + state + "]";
	}
}
