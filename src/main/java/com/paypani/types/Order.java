package com.paypani.types;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class Order {

	@JsonProperty("userid")
	private Long userId;

	@JsonIgnore
	private Long orderId;
	
	private String mobile;

	@JsonProperty("quantity")
	private Long quantity;

	@JsonIgnore
	private Date creationDate;

	@JsonIgnore
	private Date deliveredDate;

	@JsonIgnore
	private Status orderStatus;

	@JsonProperty("address")
	private Address deliveryAddress;

	public static enum Status {
		PENDING, PROCESSING, DELIVERED;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public Status getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Status orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Address getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	
	@Override
	public String toString() {
		return "Order [userId=" + userId + ", orderId=" + orderId + ", quantity=" + quantity + ", creationDate=" + creationDate + ", deliveredDate="
				+ deliveredDate + ", orderStatus=" + orderStatus + ", deliveryAddress=" + deliveryAddress + "]";
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	
}
