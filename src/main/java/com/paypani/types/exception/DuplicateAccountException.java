package com.paypani.types.exception;

public class DuplicateAccountException extends Exception{

	public DuplicateAccountException() {
		super("Account already exists");
	}
	
	public static void main(String[] args) {
		String a = "anuj";
		String b = null;
		System.out.println(a == null && b== null ? 0 : (a==null ? -1 : 1));
	}

}
