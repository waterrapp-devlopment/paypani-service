package com.paypani.types.exception;

/**
 * Exception for invalid credential.
 */
public class InvalidCredentialException extends Exception {

    public InvalidCredentialException() {
    }

    public InvalidCredentialException(String s) {
        super(s);
    }

    public InvalidCredentialException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public InvalidCredentialException(Throwable throwable) {
        super(throwable);
    }
}
