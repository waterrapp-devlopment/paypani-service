package com.paypani.types.exception;

public class InvalidPasswordException extends InvalidCredentialException{
	
	public InvalidPasswordException(){
		super("Invalid password");
	}

}
