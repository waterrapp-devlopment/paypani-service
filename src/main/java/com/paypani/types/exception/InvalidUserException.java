package com.paypani.types.exception;

public class InvalidUserException extends InvalidCredentialException{
	
	public InvalidUserException(){
		super("Invalid email or mobile");
	}
	
	public InvalidUserException(String message){
		super(message);
	}
}
