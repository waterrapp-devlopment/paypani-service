package com.paypani.types.response;

import com.paypani.db.entity.Brand;

/**
 *  Possible error codes for brand service
 */
public class BrandServiceResponse {

    public static final Long SUCCESS_STATUS_CODE = new Long(1);
    public static final String SUCCESS_STATUS_MSG = "Success Status";

    public static final Long DUPLICATE_BRAND = new Long(-1000);
    public static final String DUPLICATE_BRAND_STATUS_MSG = "Brand Name already exists";

    public static final Long OTHER_ERROR = new Long(400);
    public static final String OTHER_ERROR_STATUS_CODE = "Generic Error";

    private Long statusCode;

    private String statusMsg;

    public BrandServiceResponse(Long statusCode, String statusMsg){
        this.statusCode = statusCode;
        this.statusMsg = statusMsg;
    }

    public BrandServiceResponse(){}

    public Long getStatusCode() {
        return statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusCode(Long statusCode) {
        this.statusCode = statusCode;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }
}
