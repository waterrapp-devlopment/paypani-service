package com.paypani.types.response;

import java.util.Map;

/**
 *
 */
public class FailedServiceResponse implements ServiceResponse {

    private String message = ServiceResponse.MSG_FINGERPRINT_ERROR;

    private String code = ServiceResponse.CODE_FINGERPRINT_ERROR;

    public FailedServiceResponse(){}

    public FailedServiceResponse(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getStatusMessage() {
        return message;
    }

    public String getResponseCode() {
        return code;
    }

    public Map<String, Object> getData() {
        return null;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
