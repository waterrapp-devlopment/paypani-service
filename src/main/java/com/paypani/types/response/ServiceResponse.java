package com.paypani.types.response;

import java.util.Map;

/**
 * Service Response
 */
public interface ServiceResponse {

    public String getStatusMessage();

    public String getResponseCode();

    public Map<String, Object> getData();

    public static final String CODE_SUCCESS = "1";
    public static final String MSG_SUCCESS = "OK";

    public static final String CODE_IDENTIFICATION_ERROR = "-1000";
    public static final String MSG_IDENTIFICATION_ERROR = "Invalid credentials for identification";
    //
    public static final String CODE_FINGERPRINT_ERROR = "-1100";
    public static final String MSG_FINGERPRINT_ERROR = "Invalid fingerprint";

    public static final String CODE_INVALID_REQUEST_ERROR = "-1200";
    public static final String MSG_INVALID_REQUEST_ERROR = "Invalid/Missing Request Parameters";
    
    public static final String CODE_GENERIC_ERROR = "500";
    public static final String MSG_GENERIC_ERROR = "Generic Error";
    
    public static final String CODE_DUPLICATE_ERROR = "500";
    public static final String MSG_DUPLICATE_ERROR = "Duplicate Account Error";
    
    public static final String CODE_INVALID_USER_ERROR = "-1200";
    public static final String MSG_INVALID_USER_ERROR = "No User Found";


}
