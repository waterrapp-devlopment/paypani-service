package com.paypani.types.response;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class SuccessServiceResponse implements ServiceResponse {

    private String message;

    private String code;

    private final Map<String, Object> data = new HashMap<String, Object>();

    public String getStatusMessage() {
        return ServiceResponse.MSG_SUCCESS;
    }

    public String getResponseCode() {
        return ServiceResponse.CODE_SUCCESS;
    }

    public Map<String, Object> getData(){
        return data;
    }

}
