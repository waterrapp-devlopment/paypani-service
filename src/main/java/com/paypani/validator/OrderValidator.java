package com.paypani.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.paypani.types.Order;
import com.paypani.types.exception.InvalidParameterException;

public class OrderValidator implements Validator<Order>{
	
	private static String mobileRegex = "(\\d{10})";
	
	private static Pattern p = Pattern.compile(mobileRegex);

	@Override
	public void validate(Order order) throws Exception {
		Matcher m = p.matcher(order.getMobile());
		if(order == null || order.getMobile() == null || order.getMobile().isEmpty() || !m.matches() || order.getDeliveryAddress() == null || ! (order.getQuantity() > 0 )){
			throw new InvalidParameterException("Missing required parameter");
		}
	}

}
