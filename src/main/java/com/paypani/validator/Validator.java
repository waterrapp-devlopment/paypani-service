package com.paypani.validator;

public interface Validator<T> {
	
	public void validate(T object) throws Exception;

}
